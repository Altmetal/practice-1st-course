#include <stdio.h>

int main()
{
	int f, d;
	float a;
	printf("Enter growth\n");
	scanf("%i'%i", &f, &d);
	if ((f<0) || (d<0) || (d>11)) printf("Error\n");
	else a = (f * 12 * 2.54) + (d*2.54);
	printf("in %i ft and %i inch - %f sm\n", f, d, a);
	return 0;
}

