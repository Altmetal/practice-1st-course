#include <stdio.h>
#include <string.h>

int main()
{
	int j, i = 0;
	int count = 0;
	int s, temp;
	char str[250]="\0";
	puts("Enter your string");
	scanf("%d", &s);
	while (s != 0)
	{
		str[i] = s % 10 + '0';
		count++;
		i++;
		if (count % 3 == 0)
		{
			str[i] = ' ';
			i++;
		}
		s = s / 10;
	}
	for (i = 0, j = strlen(str) - 1; i < j; i++, j--)
	{
		temp = str[j];
		str[j] = str[i];
		str[i] = temp;
	}
	printf("%s\n", str);
}