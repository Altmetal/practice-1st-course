#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	char pass[255];
	char g;
	int i, plen;
	srand(time(0));
	puts("Enter length of your password");
	scanf("%i", &plen);
	printf("Your password is ");
	for (i = 0; i<plen; i++)
	{
		switch (rand() % 3)
		{
		case 0: printf("%c", rand() % 10 + '0');
			break;
		case 1: printf("%c", rand() % 26 + 'A');
			break;
		case 2: printf("%c", rand() % 26 + 'a');
			break;
		}
	}
	printf("\n ");
	return 0;
}

