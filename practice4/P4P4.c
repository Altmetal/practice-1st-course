#include <stdio.h>
#include <stdlib.h>
#include <time.h>

char **enterlines(int *count);
int main()
{
	char **arr;
	int count = 0, i = 0, ra, rb, tmp;
	arr = enterlines(&count);
	srand(time(0));
	for (i = 0; i <= count - 1; i++)
	{
		ra = rand() % (count+1);
		rb = rand() % (count+1);
		tmp = arr[ra];
		arr[ra] = arr[rb];
		arr[rb] = tmp;
	}
	for (i = 0; i <= count; i++)
		printf("%s", arr[i]);
	return 0;
}