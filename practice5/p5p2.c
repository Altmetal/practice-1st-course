#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct BOOK
{
	char title[30];
	char author[50];
	int year;
};

typedef struct BOOK SBOOK;

void printb(SBOOK *ptr)
{
	printf("Title:%s", ptr->title);
	printf("Author:%s", ptr->author);
	printf("Year:%d", ptr->year);
	printf("\n");
}

int main()
{
	FILE *fp;
	SBOOK *arr;
	int count = 10, i = 0, j, word = 1,max=0,min=1000,maxyear=0,minyear=3000;
	char buf[50];
	fp = fopen("books.txt", "r");
	if (fp == NULL)
	{
		perror("Mem error");
		exit(1);
	}
	arr = (SBOOK*)malloc(count*sizeof(SBOOK));
	while (fgets(buf, 50, fp))
	{
		if (i == count - 1)
			arr = (SBOOK*)realloc(arr, sizeof(SBOOK)*(count += 10));
		if (word == 1)
		{
			strcpy(arr[i].title, buf);
			word++;
		}
		else if (word == 2)
		{
			strcpy(arr[i].author, buf);
			word++;
		}
		else if (word == 3)
		{
			arr[i].year = atoi(buf);
			if (arr[i].year > maxyear)	{
											maxyear = arr[i].year;
											max = i;
										}		

			 if (arr[i].year < minyear)		{
												minyear = arr[i].year;
												min = i;
											}
			word = 1;
			i++;
		}
	}
	puts("Newest book");
	printb(&arr[max]);
	printf("\n");
	puts("Oldest book");
	printb(&arr[min]);
	return 0;
}

