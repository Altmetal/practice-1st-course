#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <ctype.h>

struct statistics
{
	int countofsymbols;
	int countwords;
	int countz;
	int countnum;
	int midword;
	char famoussymbol;
};


int main()
{
	FILE *fp;
	statistics st;
	char buf[256];
	int arr[256] = { 0 };
	int inword = 0;
	int lcount = 0;
	int max = 0;
	int maxi=0,i;
	int alllcount = 0;
	fp = fopen("text.txt", "rt");
	if (fp == NULL)
	{
		perror("Mem error");
		exit(1);
	}
	st.countofsymbols = 0;
	st.countwords = 0;
	st.countz = 0;
	st.countnum = 0;
	st.midword = 0;
	while (fgets(buf, 256, fp))
	{
		st.countofsymbols += strlen(buf);
		char *p = buf;

			

		while (*p)
		{
			arr[*p]++;

			if ((*p == ' ') || (*p == '\t') || (*p == '\n')) lcount++;
			if (isdigit(*p)) st.countnum++;
			if (ispunct(*p)) st.countz++;
			if (((*p!=' ') || (*p!='\t') || (*p != '\n')) && (inword == 0))
			{
				inword = 1;
				st.countwords++;
			}
			else
					if (((*p == ' ') || (*p == '\t') || (*p == '\n')) && (inword == 1))
						inword = 0;
			p++;
		}
	}

	for (i = 0; i < 256; i++)
	if (arr[i]>max)
	{
		max = arr[i];
		maxi = i;
	}
	st.famoussymbol = maxi;

	st.midword = (st.countofsymbols-lcount) / st.countwords;

	printf("Number of symbols = %i\n", st.countofsymbols);
	printf("Number of words = %i\n", st.countwords);
	printf("Number of punctuation = %i\n", st.countz);
	printf("Number of digits = %i\n", st.countnum);
	printf("The average length of a word = %i\n", st.midword);
	printf("The most popular symbol = '%c'\n", st.famoussymbol);
	
	return 0;
}