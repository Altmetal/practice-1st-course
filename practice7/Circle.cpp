#include "Circle.h"
#include <math.h>
#define sqr(x) (x*x)
#define pi 3.14159265359
void Circle::set_radius(double value)
{
	radius = value;
}
void Circle::set_ference(double value)
{
	ference = value;
}
void Circle::set_area(double value)
{
	area = value;
}
void Circle::calc_ference_area()
{
	ference = 2 * pi * radius;
	area = pi * sqr(radius);
}
void Circle::calc_radius_area()
{
	radius = ference / (2 * pi);
	area = pi * sqr(radius);
}
void Circle::calc_radius_ference()
{
	radius = sqrt(area / pi);
	ference = 2 * pi * radius;
}
double Circle::get_radius()
{
	return radius;
}
double Circle::get_ference()
{
	return ference;
}
double Circle::get_area()
{
	return area;
}



