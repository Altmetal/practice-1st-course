class Circle
{
private:
	double radius;
	double ference;
	double area;
public:
	void set_radius(double);
	void set_ference(double);
	void set_area(double);
	void calc_ference_area();
	void calc_radius_ference();
	void calc_radius_area();
	double get_radius();
	double get_ference();
	double get_area();
};