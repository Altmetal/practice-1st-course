#include "Automat.h"

Automat::Automat(void)
{
	FILE *Menu;
	FILE *Prices;
	char bufm[30];
	char bufp[5];
	int i = 0, k = 0;

	cm = 0;
	cp = 0;

	state = OFF;

	Menu = fopen("menu.txt", "r");

	if (Menu == NULL)
	{
		perror("File error (menu)");
		exit(1);
	}

	Prices = fopen("prices.txt", "r");

	if (Prices == NULL)
	{
		perror("File error (prices)");
		exit(1);
	}

	while (fgets(bufm, 30, Menu))
	{
		cm++;
	}

	while (fgets(bufp, 5, Prices))
	{
		cp++;
	}

	if (cp != cm)
	{
		puts("Values of price and menu are not the same");
		Sleep(3000);
		exit(1);
	}

	menu = (char**)malloc(cm*sizeof(char));
	for (int j = 0; j < cm; j++)
		menu[j] = (char*)malloc(30 * sizeof(char));
	prices = (int*)malloc(cp*sizeof(int));

	rewind(Menu);
	rewind(Prices);

	while (fgets(bufm, 30, Menu))
	{
		strcpy(menu[i], bufm);
		i++;
	}

	while (fgets(bufp, 5, Prices))
	{
		prices[k] = atoi(bufp);
		k++;
	}
}


void Automat::choice(int value)
{
	choose = value;
}
bool Automat::check()
{
	state = CHECK;
	printstate();
	if (cash < prices[choose])
	{
		puts("Not enough money\n");
		return false;
	}
	else return true;

}

void Automat::off()
{
	state = OFF;
	puts("Turning off...");
	Sleep(1000);
	exit(1);
}

void Automat::on()
{
	state = WAIT;
	printf("\n");
	printstate();
	cash = 0;
}

void Automat::printmenu()
{
	for (int i = 0; i < cp; i++)
	{
		printf("%i: %s - %i p\n", i, menu[i], prices[i]);
	}
	printf("\n");
}

void Automat::coin(int value)
{
	cash += value;
	state = ACCEPT;
	printf("\n");
	printstate();
	printf("You have %i �. on your account \n", cash);
}

void Automat::cook()
{
	state = COOK;
	printstate();
	printf("Wait until your drink will be ready\n");
	for (int i = 0; i < 3; i++)
	{
		Sleep(2000);
		printf(".");
	}
	printf("\n");
}

void Automat::finish()
{
	printf("Your drink is ready\n");
	if (cash>prices[choose])
	{
		cash = cash - prices[choose];
		printf("Do not forget to take your change %i p !\n", cash);
	}
	for (int i = 0; i < 3; i++)
		Sleep(2000);
	state = WAIT;
	printstate();

}

void Automat::printstate()
{
	printf("\n");
	switch (state)
	{
		case 0: puts("status: OFF"); break;
		case 1: puts("status: WAIT"); break;
		case 2: puts("status: ACCEPT"); break;
		case 3: puts("status: CHECK"); break;
		case 4: puts("status: COOK"); break;
	}
	printf("\n");
}

STATES Automat::getstate()
{
	return state;
}

void Automat::cancel()
{
	if ((state == ACCEPT) | (state == CHECK))
	{
		state = WAIT;
		printf("You have %i �. on your account \n", cash);
	}
	else puts("Not Avaiable");
}