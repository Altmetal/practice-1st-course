
#include <stdio.h>
#include <Windows.h>
#include <string>
#include <stdlib.h>

using namespace std;

enum STATES { OFF, WAIT, ACCEPT, CHECK, COOK };

class Automat
{
private:
	int cash;
	char **menu;
	int *prices;
	STATES state;
	int choose;
	int cp,cm;
public:
	Automat();
	void on();
	void off();
	void coin(int);
	void printmenu();
	STATES getstate();
	void printstate();
	void choice(int);
	bool check();
	void cancel();
	void cook();
	void finish();
};