#include "Matrix.h"
#include <iostream>
#include <stdio.h>
#include<conio.h>
using namespace std;

Matrix operator*(double value, const Matrix& mtx)
{
	Matrix result(mtx.N, mtx.M);

	for (int i = 0; i < mtx.N; i++)
		for (int j = 0; j < mtx.M; j++)
		result.data[i][j] = value * mtx.data[i][j];

	return result;
}

int main()
{
	int N, M;
	char filename[20];


	cout << "Hello! This is demonstration program. You can pre-order full version now for 9.99$" << endl;
	cout << endl;
	cout << "Please, input number of lines in matrix A:" << endl;
	cin >> N;
	cout << endl;
	cout << "Input number of collums in matrix A:" << endl;
	cin >> M;
	cout << endl;
	
	Matrix A(N, M);
	Matrix B;

	cout << "Please, input name of file with matrix B:" << endl;
	cin >> filename;

	cout << "Matrix A:" << endl;
	A.randM();
	A.printM();
	cout << endl;

	cout << "Matrix B:" << endl;
	B.Load(filename);
	B.printM();
	cout << endl;

	int choise;
	char ch;
	double num, det;
	Matrix C;

	while (1)
	{
		cout << endl;
		cout << "Choose operation" << endl;
		cout << "1 - A+B" << endl;
		cout << "2 - A-B" << endl;
		cout << "3 - A*B" << endl;
		cout<< "4 - matrix * number" << endl;
		cout << "5 - matrix / number" << endl;
		cout << "6 - testing for equality" << endl;
		cout<< "7 - print a element of matrix" << endl;
		cout << "8. determinant of matrix" << endl;
		cout << "9. reverse matrix" << endl;
		cout<< "10. transpose matrix" << endl;
		cout << "11. save to file" << endl;
		cout<< "12. exit" << endl;
		cin >> choise;


		switch (choise)
		{
		default: cout << "Input error! Try again!" << endl; break;
		case 1:
			C = A + B;
			C.printM();
			break;
		case 2:
			C = A - B;
			C.printM();
			break;
		case 3:
			C = A * B;
			C.printM();
			break;
		case 4:
			cout << "if you want to multiply the matrix A - print A,  matrix B - print B" << endl;
			ch = getche();
			cout << endl;
			if ((ch == 'A') || (ch == 'a'))
			{
				cout << "Enter the number to which you want to multiply the matrix A:" << endl;
				num = getche() - 48;
				cout << endl;
				C = A * num;
				C.printM();
			}
			else
			{
				cout << "Enter the number to which you want to multiply the matrix B:" << endl;
				num = getche() - 48;
				cout << endl;
				C = B * num;
				C.printM();
			}

			break;
		case 5:
			cout << "if you want to divide the matrix A - print A,  matrix B - print B" << endl;
			ch = getche();
			cout << endl;
			if ((ch == 'A') || (ch == 'a'))
			{
				cout << "Enter the number to which you want to divide the matrix A:";
				num = getche() - 48;
				cout << endl;
				C = A / num;
				C.printM();
			}
			else
			{
				cout << "Enter the number to which you want to divide the matrix B:";
				num = getche() - 48;
				cout << endl;
				C = B / num;
				C.printM();
			}
			break;
		case 6:
			if (A == B)
				cout << "Matrix are equal" << endl;
			else
				cout << "Matrix are not equal" << endl;
			break;
		case 7:
			cout << "if you want to see element of matrix A - print A, of matrix B - print B" << endl;
			ch = getche();
			cout << endl;
			if ((ch == 'A') || (ch == 'a'))
			{
				cout << "Enter the number of element in matrix A:" << endl;
				cin >> N >> M;
				cout << endl << A(N, M) << endl;
			}
			else
			{
				cout << "Enter the number of element in matrix A:" << endl;
				cin >> N >> M;
				cout << endl << B(N, M) << endl;
			}
			break;
		case 8:
			cout << "if you want to calculate the determinant of matrix A - print A, of matrix B - print B" << endl;
			ch = getche();
			cout << endl;
			if ((ch == 'A') || (ch == 'a'))
			{
				det = A.det();
				cout << det << endl;
			}
			else 
			{
				det = B.det();
				cout << det << endl;
			}
			break;
		case 9:
			cout << "if you want to calculate the reverse matrix of matrix A - print A, of matrix B - print B" << endl;
			ch = getche();
			cout << endl;
			if ((ch == 'A') || (ch == 'a'))
			{
				C = A.reverse();
				C.printM();
			}
			else
			{
				C = B.reverse();
				C.printM();
			}
			break;
		case 10:
			cout << "if you want to calculate the transpose matrix of matrix A - print A, of matrix B - print B" << endl;
			ch = getche();
			cout << endl;
			if ((ch == 'A') || (ch == 'a'))
			{
				C = A.transp();
				C.printM();
			}
			else
			{
				C = B.transp();
				C.printM();
			}
			break;
		case 11:
			cout << "Saving result" << endl;
			C.Save();
			cout << endl;
			cout << "done" << endl;
			break;
		case 12: exit(1);
		}
	}
	return 0;
}